﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tri_par_ordre_alphabétique
{
    class Program
    {
        static Dictionary<int, char> odicoLettreAccent;
        static void Main(string[] args)
        {
            // Dictionnaire permettant de faire la correspondance entre les caractères accentués et le caractère sans accent qui correspond
            odicoLettreAccent = new Dictionary<int, char>();
            odicoLettreAccent.Add(201, 'e'); // Code ASCII ISO-8859-1 pour la lettre 'É'
            odicoLettreAccent.Add(232, 'e'); // Code ASCII ISO-8859-1 pour la lettre 'è'
            odicoLettreAccent.Add(233, 'e'); // Code ASCII ISO-8859-1 pour la lettre 'é'
            odicoLettreAccent.Add(235, 'e'); // Code ASCII ISO-8859-1 pour la lettre 'ë'

            // Déclation et peuplement du tableau
            string[] tabNom = new string[10];
            tabNom[0] = "Zoé";
            tabNom[1] = "Léa";
            tabNom[2] = "Aglaé";
            tabNom[3] = "Chantal";
            tabNom[4] = "Michaël";
            tabNom[5] = "Zaccharia";
            tabNom[6] = "Marine";
            tabNom[7] = "Éleonore";
            tabNom[8] = "Véronique";
            tabNom[9] = "Marion";

            // Appel de la procédure tri à laquelle je passe la référence du tableau original
            tri(ref tabNom);

            // Affichage du résultat trié
            for (int i = 0; i < tabNom.Length; i++)
            {
                Console.Write(Convert.ToString(i + 1) + " : ");
                Console.WriteLine(tabNom[i] + "\n\r");
            }

            Console.ReadKey();


        }

        static void tri(ref string[] stabAtrier)
        {
            bool estTrie = false; // Pour optimiser
            for (int x = 0; x < stabAtrier.Length - 1 && !estTrie; x++) //double condition d'arrêt de la boucle for (optimisation)
            {
                estTrie = true; // Si n'est pas mis à false lors de la remontée de la bulle alors reste TRUE et on sort alors de la boucle externe

                for (int i = 0; i < stabAtrier.Length - 1; i++) // Boucle interne de remontée des "bulles"
                {
                    char[] X1 = null;
                    char[] X2 = null;
                    X1 = stabAtrier[i].Substring(0, 1).ToCharArray();
                    X2 = stabAtrier[i + 1].Substring(0, 1).ToCharArray();
                    char lettreSansAccentX1;
                    bool oKX1 = false, oKX2 = false;
                    oKX1 = odicoLettreAccent.TryGetValue(X1[0], out lettreSansAccentX1);
                    char lettreSansAccentX2;
                    oKX2 = odicoLettreAccent.TryGetValue(X2[0], out lettreSansAccentX2);
                    if (oKX1) { X1[0] = lettreSansAccentX1; }
                    if (oKX2) { X2[0] = lettreSansAccentX2; }

                    if (Char.ToLower(X1[0]) > Char.ToLower(X2[0]))
                    {
                        permutation(stabAtrier, i);
                        estTrie = false;
                    }

                    if (Char.ToLower(X1[0]) == Char.ToLower(X2[0]))
                    {
                        for (int z = 1; z < stabAtrier[i].Length; z++)
                        {
                            char[] X = null;
                            char[] Y = null;
                            X = stabAtrier[i].Substring(z, 1).ToCharArray(); // n'est jamais en erreur sur la longueur puisque on tourne sur la longueur de stabAtrier[i]
                            try { Y = stabAtrier[i + 1].Substring(z, 1).ToCharArray(); } // est en erreur ou pas tout dépend de la longueur 
                            catch
                            {
                                Y = new char[1];
                                Y[0] = '!';
                            } // Pour la lettre absente je met un caractère dont le code ascii est inférieur à la lettre 'a'

                            /* Traitement du problème des accents ********************************/
                            char lettreSansAccentX;
                            bool oKX = false, oKY = false;
                            oKX = odicoLettreAccent.TryGetValue(X[0], out lettreSansAccentX); // Je remplace par la même lettre sans accent
                            char lettreSansAccentY;
                            oKY = odicoLettreAccent.TryGetValue(Y[0], out lettreSansAccentY);
                            if (oKX) { X[0] = lettreSansAccentX; }
                            if (oKY) { Y[0] = lettreSansAccentY; }
                            /**********************************************************************/
                            if (Char.ToLower(X[0]) > Char.ToLower(Y[0]))
                            {
                                // Cas où la lettre recontré du premier est après celle du second , il faut permuter
                                permutation(stabAtrier, i);
                                estTrie = false;
                                break;
                            }
                            else
                            {
                                if (Char.ToLower(X[0]) < Char.ToLower(Y[0]))
                                {
                                    //cas ou la lettre du premier est avant dans l'alphabet, il faut sortir c'est déjà trié.
                                    break;
                                }

                            }
                        }


                    }
                }
                //if (estTrie) { break; /* Si n'a pas permuté alors le tableau est tri donc on sort et on renvoie le tableau */ }
            }
            //return stabAtrier;
        }



        static void permutation(string[] stab, int si) // Pas besoin du mot clef 'ref' car un tableau est un type référence 
        {
            string temp = stab[si]; // Je travaille donc avec l'original
            stab[si] = stab[si + 1];
            stab[si + 1] = temp;

        }


    }
}
